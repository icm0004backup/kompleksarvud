#!/usr/bin/env python3

import kompleksarv
import unittest
import math


class MyTestCase(unittest.TestCase):

    def test_eq(self):
        res1 = kompleksarv.Kompleksarv(1, 0)
        res2 = kompleksarv.Kompleksarv(0, 1)
        self.assertNotEqual(res1, res2, "1 ei ole v6rdne 1i")
        res1 = kompleksarv.Kompleksarv(1, 0)
        res2 = kompleksarv.Kompleksarv(0, 0)
        self.assertNotEqual(res1, res2, "1 ei ole v6rdne 0")

    def test_round(self):
        r1 = math.pi * math.e * 1000000.
        r2 = r1 / math.pi / 1000.
        r3 = r2 / math.e / 1000.
        k1 = kompleksarv.Kompleksarv(r3, 1/r3)
        k2 = kompleksarv.Kompleksarv(1/r3, r3)
        self.assertEqual(k1, k2, "ymardamise viga")

    def test_plus(self):
        res = kompleksarv.Kompleksarv(2, 5).__add__(kompleksarv.Kompleksarv(4, 15))
        self.assertEqual(kompleksarv.Kompleksarv(6, 20), res, "liitmise viga")
        res = kompleksarv.Kompleksarv(-2, -5).__add__(kompleksarv.Kompleksarv(4, 15))
        self.assertEqual(kompleksarv.Kompleksarv(2, 10), res, "liitmise viga")

    def test_minus(self):
        res = kompleksarv.Kompleksarv(2, 5).__sub__(kompleksarv.Kompleksarv(4, 15))
        self.assertEqual(kompleksarv.Kompleksarv(-2, -10), res, "lahutamise viga")
        res = kompleksarv.Kompleksarv(-2, -5).__sub__(kompleksarv.Kompleksarv(4, 15))
        self.assertEqual(kompleksarv.Kompleksarv(-6, -20), res, "lahutamise viga")

    def test_mult(self):
        res = kompleksarv.Kompleksarv(4, -3).__mul__(kompleksarv.Kompleksarv(-24, 7))
        self.assertEqual(kompleksarv.Kompleksarv(-75, 100), res, "korrutamise viga")
        res = kompleksarv.Kompleksarv(1, -1).__mul__(kompleksarv.Kompleksarv(3, 7))
        self.assertEqual(kompleksarv.Kompleksarv(10, 4), res, "korrutamise viga")

    def test_div(self):
        res = kompleksarv.Kompleksarv(-75, 100).__truediv__(kompleksarv.Kompleksarv(-24, 7))
        self.assertEqual(kompleksarv.Kompleksarv(4, -3), res, "jagamise viga")
        res = kompleksarv.Kompleksarv(10, 4).__truediv__(kompleksarv.Kompleksarv(1, -1))
        self.assertEqual(kompleksarv.Kompleksarv(3, 7), res, "jagamise viga")

    def test_div_by_zero(self):
        with self.assertRaises(Exception, msg="nulliga jagamine peaks olema viga"):
            kompleksarv.Kompleksarv(3, 4).__truediv__(kompleksarv.Kompleksarv(0, 0))

    def test_nurk_null(self):
        with self.assertRaises(Exception, msg="nullist nurga leidmine peaks olema viga"):
            kompleksarv.Kompleksarv(0, 0).nurk()

    def test_nurk(self):
        for i in range(0, 360):
            c1 = kompleksarv.Kompleksarv(math.cos(math.radians(i)), math.sin(math.radians(i)))
            m = c1.moodul()
            p = c1.nurk()
            self.assertEqual(c1, kompleksarv.Kompleksarv(m*math.cos(p), m*math.sin(p)), "nurga viga")

    def test_parse(self):
        for i in range(-1, 2):
            for j in range(-1, 2):
                c1 = kompleksarv.Kompleksarv(i, j)
                s1 = c1.__str__()
                c2 = kompleksarv.Kompleksarv.parse_kompleksarv(s1)
                self.assertEqual(c1, c2, "parsimise viga")


if __name__ == '__main__':
    unittest.main()
